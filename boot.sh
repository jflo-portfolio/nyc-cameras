#!/bin/bash
source .venv/bin/activate
exec gunicorn --bind :5000 nyc_cameras.app:app --access-logfile - --error-logfile -