.PHONY: lint format update docker-build docker-run dev serve
.DEFAULT_GOAL := default

lint:
	flake8 .
	mypy .
	pydocstyle .

format:
	black .
	isort --sl .

update:
	python scripts/get_cameras.py --output scripts/cameras_list.txt
	python scripts/init_db.py -c scripts/cameras_list.txt --output scripts/cameras.db

docker-build:
	docker build -t nyc-cameras .

docker-run:
	docker run -d --name nyc-cameras --rm -p 5000:5000 nyc-cameras

dev:
	source dev.sh; flask run

serve:
	gunicorn --bind 0.0.0.0:5000 nyc_cameras.app:app --access-logfile - --error-logfile - 

default:
	@echo "nyc cameras"
