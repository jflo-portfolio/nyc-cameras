# nyc-cameras

nyc-cameras is a Flask app that displays images from the NYC Department of Transportation's real-time traffic cameras.

![nyc cameras](imgs/nyc-cameras.png)

## Installation
Clone the repo:

git clone https://bitbucket.org/jflo-portfolio/nyc-cameras.git


### Python version

Python (>= 3.7)


## Run Application w/ Docker

type: `make docker-build` to build the docker image

type: `make docker-run` to run the docker container

## Web App
You can go to: http://localhost:5000/
for the web app.

## API
You can go to: http://localhost:5000/api/cameras/
for the the api.

### Examples:
http://localhost:5000/api/cameras/?borough=brooklyn

http://localhost:5000/api/cameras/?limit=100