FROM python:3.9-slim-buster

RUN useradd nyc-cameras

WORKDIR /home/nyc-cameras

COPY . .

ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get update -y
RUN python3 -m pip install --upgrade pip
RUN pip install poetry && \
    poetry config virtualenvs.create true && \
    poetry config virtualenvs.in-project true && \
    poetry install --no-dev

RUN chmod +x boot.sh
RUN chown -R nyc-cameras:nyc-cameras ./
USER nyc-cameras

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]