#!/usr/bin/env python3
import argparse
import re
import sys
import time
from io import StringIO
from pathlib import Path
from typing import Dict
from typing import List

import requests
from lxml import html  # type: ignore

HEADERS = {
    "User-Agent": (
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) "
        "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36"
    )
}


def remove_spaces(orig_str: str) -> str:
    """Remove repetitive spaces in a string

    :param orig_str: input string
    :type orig_str: str
    :return: string with unnecessary spaced removed
    :rtype: str
    """
    new_str = re.sub(r"\s+", " ", orig_str)
    return new_str.strip()


def borough_pages_dict(
    sess: requests.Session, html_input: str
) -> Dict[str, List[Dict[str, str]]]:
    """Scrapes an html page and returns a dictionary containing borough/page info

    :param session: the current requests session
    :type session: requests.Session
    :param html: html to parse = can be a filepath or url
    :return: dictionary {borough : [{'location': location, 'page': page}]}
    :rtype: dict
    """
    html_file = Path(html_input)
    if html_file.is_file():
        with open(html_file, "r") as fobj:
            doc = html.parse(fobj)
    else:
        # is a url
        res = sess.get(html_input)
        fobj = StringIO(res.text)
        doc = html.parse(fobj)

    borough_pages = dict()
    tables = doc.xpath("//table[contains(@id, 'tableCam')]")
    for table in tables:
        borough = remove_spaces(table.xpath("tr/td/strong/font[1]")[0].text_content())
        borough = re.sub(r"[^\x00-\x7F]+", " ", borough)
        checkboxes = table.xpath("descendant::input[@type='checkbox']")
        pages = list()
        for checkbox in checkboxes:
            location = checkbox.xpath("following::span[1]")[0].text_content()
            page_url = "https://nyctmc.org/multiview2.php?listcam={}".format(
                checkbox.get("value")
            )
            pages.append({"location": location, "page": page_url})
        borough_pages[borough] = pages
    return borough_pages


def get_camera_url(session: requests.Session, page: str) -> str:
    """_summary_

    :param session: the current requess session
    :type session: requests.Session
    :param page: html page to parse - url
    :type page: str
    :return: URL of the camera page
    :rtype: str
    """
    res = session.get(page, headers=HEADERS)
    fobj = StringIO(res.text)
    doc = html.parse(fobj)
    camera_url = str(doc.xpath("//img[@id='repCamView__ct0_imgLink'][1]")[0].get("src"))
    return camera_url


def main() -> None:
    """Script that creates a text file containing nyc cameras"""
    parser = argparse.ArgumentParser(description="Get cameras list")
    parser.add_argument("-d", "--delay", default=0, type=int)
    parser.add_argument("-o", "--output", default="cameras_list.txt", type=str)
    args = parser.parse_args()

    output_file = Path(args.output)
    if output_file.exists():
        ans = input("File exists. Overwrite {}?\n".format(str(output_file)))
        if ans.lower() == "yes" or ans.lower() == "y":
            output_file.unlink()
        else:
            sys.exit(0)

    sess = requests.Session()
    borough_pages = borough_pages_dict(sess, "https://nyctmc.org/multiview2.php")
    with open(output_file, "w") as f:
        for borough, pages in borough_pages.items():
            for page in pages:
                camera_url = get_camera_url(sess, page["page"])
                cam_string = "{} | {} | {} | {}".format(
                    borough, page["location"], page["page"], camera_url
                )
                print(cam_string)
                f.write(cam_string + "\n")
                time.sleep(args.delay)
    f.close()


if __name__ == "__main__":
    main()
