#!/usr/bin/env python3
import argparse
import os
import sqlite3
import sys
from pathlib import Path

SQL_FILE = "create_tables.sql"


def main() -> None:
    """Script to initialize the database"""
    parser = argparse.ArgumentParser(description="Initialize database")
    parser.add_argument("-o", "--output", default="cameras.db", type=str)
    parser.add_argument("-c", "--cameras-list", default="cameras_list.txt", type=str)
    args = parser.parse_args()

    db_path = Path(args.output)

    if db_path.exists():
        ans = input("File exists. Overwrite {}?\n".format(str(db_path)))
        if ans.lower() == "yes" or ans.lower() == "y":
            db_path.unlink()
        else:
            sys.exit(0)

    con = sqlite3.connect(db_path)
    cur = con.cursor()
    fpath = os.path.dirname(os.path.realpath(__file__))
    sqlpath = os.path.join(fpath, SQL_FILE)

    # create db
    with open(Path(sqlpath)) as f:
        sql_script = f.read()

    cur.executescript(sql_script)

    # populate db
    cam_list = Path(args.cameras_list)
    with open(Path(cam_list)) as f:
        lines = f.readlines()

    for line in lines:
        camera_data = line.strip().split(" | ")
        sql_line = (
            "INSERT INTO cameras ('borough', 'street', 'camera_url') "
            "VALUES ('{0}', '{1}', '{2}')"
        ).format(camera_data[0], camera_data[1], camera_data[3])
        # print(sql_line)
        cur.execute(sql_line)
        con.commit()
    con.close()


if __name__ == "__main__":
    main()
