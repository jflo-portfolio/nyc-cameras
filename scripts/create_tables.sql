-- Table that stores camera data
CREATE TABLE cameras (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  borough TEXT NOT NULL,
  street TEXT NOT NULL,
  camera_url TEXT NOT NULL
);

-- As SQLite index content on a virtual table,
CREATE VIRTUAL TABLE cameras_index USING fts5(borough, street, camera_url, tokenize=porter);

-- Trigger on CREATE
CREATE TRIGGER after_cameras_insert AFTER INSERT ON cameras BEGIN
  INSERT INTO cameras_index (
    rowid,
    borough,
    street,
    camera_url
  )
  VALUES(
    new.id,
    new.borough,
    new.street,
    new.camera_url
  );
END;

-- Trigger on UPDATE
CREATE TRIGGER after_cameras_update UPDATE OF borough, street, camera_url ON cameras BEGIN
  UPDATE camras_index SET borough = new.borough, street = new.street, camera_url = new.camera_url WHERE rowid = old.id;
END;

-- Trigger on DELETE
CREATE TRIGGER after_cameras_delete AFTER DELETE ON cameras BEGIN
    DELETE FROM cameras_index WHERE rowid = old.id;
END;

