#!/usr/bin/env python3
import sqlite3
from typing import Any
from typing import Dict
from typing import List
from typing import NamedTuple
from typing import Tuple
from typing import Union

BOROUGHS = ["manhattan", "brooklyn", "queens", "staten island"]


class CamsData(NamedTuple):
    """Represents camera results retrieved from db

    :param total: total number of cameras
    :type total: int
    :param rows: list of camera rows - (borough, street, camera_url)
    :type rows: List[Tuple[str, str, str]]
    """

    total: int
    rows: List[Tuple[str, str, str]]


class PageNavItem(NamedTuple):
    """Represents page navigation item

    :param display: display the page item - either "<" or int or ">"
    :type display: Union[str, int]
    :param start_item: which camera to start with
    :type start_item: int
    :param is_active: is page the current one
    :type is_active: bool
    """

    display: Union[str, int]
    start_item: int
    is_active: bool


def get_cameras(
    query: str = "", borough: str = "", start: int = 0, limit: int = 9
) -> CamsData:
    """Gets cameras data based on query/borough

    :param query: query to search for, defaults to ""
    :type query: str, optional
    :param borough: borough, defaults to ""
    :type borough: str, optional
    :param start: index number of camera to start with, defaults to 0
    :type start: int, optional
    :param limit: number of cameras to query, defaults to 9
    :type limit: int, optional
    :return: CamsData containing the total and a list of rows
    :rtype: CamsData
    """
    con = sqlite3.connect("nyc_cameras/data/cameras.db")
    cur = con.cursor()
    if borough:
        base_cmd = (
            "SELECT * from cameras_index WHERE street LIKE ? "
            "AND borough MATCH ? ORDER BY street"
        )
        count_cmd = base_cmd.replace("*", "COUNT(*)")
        sql_cmd = "{} ASC LIMIT ? OFFSET ?".format(base_cmd)
        cur.execute(count_cmd, ("%{}%".format(query), borough))
        row = cur.fetchone()
        count = row[0]
        cur.execute(sql_cmd, ("%{}%".format(query), borough, limit, start))
        rows = cur.fetchall()
    else:
        base_cmd = "SELECT * from cameras_index WHERE street LIKE ? ORDER BY street"
        count_cmd = base_cmd.replace("*", "COUNT(*)")
        sql_cmd = "{} ASC LIMIT ? OFFSET ?".format(base_cmd)
        cur.execute(count_cmd, ("%{}%".format(query),))
        row = cur.fetchone()
        count = row[0]
        cur.execute(sql_cmd, ("%{}%".format(query), limit, start))
        rows = cur.fetchall()

    con.commit()
    con.close()
    return CamsData(count, rows)


def page_navigation(
    total: int, num_pages: int, curr_page_num: int, limit: int, buffer: int = 3
) -> List[PageNavItem]:
    """Create the page_navigation

    :param total: total number of cameras
    :type total: int
    :param num_pages: number of pages
    :type num_pages: int
    :param curr_page_num: current page number
    :type curr_page_num: int
    :param limit: number of cameras to display per page
    :type limit: int
    :param buffer: buffer, defaults to 3
    :type buffer: int, optional
    :return: list that resembles the page navigation
    :rtype: List[PageNavItem]
    """
    navigation = list()
    if curr_page_num > 1:
        navigation.append(
            PageNavItem("<", ((curr_page_num - 1) * limit) - limit, False)
        )

    buffer = buffer
    b_start = curr_page_num - (buffer + 1)
    b_end = curr_page_num + 3
    if b_start < 0:
        b_start = 0
    if b_end > num_pages:
        b_end = num_pages

    for page in range(b_start, b_end):
        is_active = False
        if (page + 1) == curr_page_num:
            is_active = True
        navigation.append(PageNavItem(page + 1, page * limit, is_active))
    if curr_page_num < num_pages:
        navigation.append(PageNavItem(">", curr_page_num * limit, False))

    return navigation


def page_data(
    total: int, items: List[Tuple[str, str, str]], start: int = 0, limit: int = 9
) -> Dict[str, Any]:
    """page data required to display the camera image info

    :param total: total number of cameras
    :type total: int
    :param items: list of camera rows - (borough, street, camera_url)
    :type items: List[Tuple[str, str, str]]
    :param start: start number of image to display, defaults to 0
    :type start: int, optional
    :param limit: number of images to display, defaults to 9
    :type limit: int, optional
    :return: data required to display camera image info: total, num_pages,
    curr_page_num, rows, navigation
    :rtype: Dict[str, Any]
    """
    num_pages, np_remainder = divmod(total, limit)
    if np_remainder > 0:
        num_pages = num_pages + 1

    if start > 0:
        curr_page_num, cp_remainder = divmod(start, limit)
        if cp_remainder >= 0:
            curr_page_num = curr_page_num + 1
    else:
        curr_page_num = 1

    for item in items:
        print(item)
        s = item[1]
        print(s)

    navigation = page_navigation(total, num_pages, curr_page_num, limit)
    print("total number of items: {}".format(total))
    print("num pages: {}".format(num_pages))
    print("curr_page_num: {}".format(curr_page_num))
    print(navigation)
    return {
        "total": total,
        "num_pages": num_pages,
        "curr_page_num": curr_page_num,
        "rows": items,
        "navigation": navigation,
    }


def main() -> None:
    """Script to diplay show nyc cameras based on query/borough

    :raises ValueError: raised if borough is incorrect
    """
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-q", "--query", default="", action="store")
    parser.add_argument("-b", "--borough", default="", action="store")
    parser.add_argument("-s", "--start", default=0, action="store")
    args = parser.parse_args()
    borough = args.borough
    query = args.query
    start = int(args.start)

    if borough and borough not in BOROUGHS:
        raise ValueError("Borough does not exist")

    cameras = get_cameras(query, borough, start=start)
    page_data(cameras.total, cameras.rows, start)


if __name__ == "__main__":
    main()
