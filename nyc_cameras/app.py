from random import random

from flask import Flask
from flask import Response
from flask import abort
from flask import jsonify
from flask import render_template
from flask import request

from . import cams

app = Flask(__name__)


@app.route("/", methods=["GET"])
def index() -> str:
    """Shows page of nyc cameras based on query/borough

    :return: html page
    :rtype: str
    """
    query = request.args.get("query", default="", type=str)
    borough = request.args.get("borough", default="", type=str)
    if borough and borough not in cams.BOROUGHS:
        abort(400)

    start = request.args.get("start", default=0, type=int)
    cameras = cams.get_cameras(query, borough, start=start)
    camera_data = cams.page_data(cameras.total, cameras.rows, start)
    return render_template(
        "index.jinja",
        title="NYC Cameras",
        query=query,
        curr_borough=borough,
        boroughs=cams.BOROUGHS,
        cameras=camera_data,
        random_sec=random(),
    )


@app.route("/api/cameras/")
def api() -> Response:
    """Cameras api

    {'total': total_num, 'results': [{'borough': borough,
                                      'street': street,
                                      'camera': camera }]}

    :return: json payload with total number of camera and results
    :rtype: flask.Response
    """
    query = request.args.get("query", default="", type=str)
    borough = request.args.get("borough", default="", type=str)
    if borough and borough not in cams.BOROUGHS:
        abort(400)

    start = request.args.get("start", default=0, type=int)
    limit = request.args.get("limit", default=9, type=int)
    cameras = cams.get_cameras(query, borough, start=start, limit=limit)
    results = [{"borough": r[0], "street": r[1], "camera": r[2]} for r in cameras.rows]
    return jsonify({"total": cameras.total, "results": results})
